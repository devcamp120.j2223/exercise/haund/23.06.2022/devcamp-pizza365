import { Component } from "react";

class Header extends Component{
    render(){
        let url ="#";
        return(
            <div className="row"> 
              <div className="col-12">
                  <nav className="navbar navbar-expand-md navbar-light fixed-top nav-fill w-100 d-flex">
                      <a href={url} className="hover-task navbar-brand text-white font-weight-bold font-italic"><i className="fas fa-drum-steelpan"></i> Pizza 365 !!</a>
                      <div className="collapse navbar-collapse justify-content-end">
                          <ul id="list-item" className="navbar-nav font-weight-bold ml-auto">
                              <li id="hover" className="nav-item">
                                  <a href={url} className="nav-link  text-white">Home</a>
                              </li>
                              <li className="nav-item">
                                  <a href={url} className="nav-link text-white">Combo</a>
                              </li>
                              <li className="nav-item">
                                  <a href={url} className="nav-link text-white">Pizza Type</a>
                              </li>
                              <li className="nav-item">
                                  <a href={url} className="nav-link text-white">Gửi đơn hàng</a>
                              </li>
                              <li className="nav-item nav">
                                  <button className="btn btn-order font-weight-bold">Order Online</button>
                              </li>
                              <li className="nav-item">
                                  <a href={url} className="nav-link text-white"><i className="fas fa-list"></i></a>
                              </li>
                          </ul>
                      </div>
                  </nav>
              </div>
          </div>
        )
    }
}

export default Header;