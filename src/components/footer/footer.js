import { Component } from "react";

class Footer extends Component {
    render() {
        let url = "#";
        return (
            <div className="footer bg-footer text-center">
                <div className="contain-footer p-4">
                    <h3 className="font-weight-bold mb-n1">Footer</h3>
                    <button className="btn btn-color text-white mt-2 mb-2"><i className="fas fa-arrow-up"></i> To the top</button>
                    <div className="icon-list  mt-2 mb-2">
                        <a href={url} className=""><i className="fab fa-facebook-f"></i></a>
                        <a href={url} className=""><i className="fab fa-twitter"></i></a>
                        <a href={url} className=""><i className="fab fa-instagram"></i></a>
                        <a href={url} className=""><i className="fab fa-pinterest"></i></a>
                        <a href={url} className=""><i className="fab fa-pagelines"></i></a>
                    </div>
                    <div className="font-weight-bold">Powered by DEVCAMP</div>
                </div>
            </div>
        )
    }
};


export default Footer;