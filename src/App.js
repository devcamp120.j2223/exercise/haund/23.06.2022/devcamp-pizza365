import { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { Carousel } from 'react-bootstrap';

import Header from './components/header/header';
import Footer from './components/footer/footer';

import slider1 from "./assets/images/1.jpg";
import slider2 from "./assets/images/2.jpg";
import slider3 from "./assets/images/3.jpg";
import slider4 from "./assets/images/4.jpg";

import seafood from "./assets/images/seafood.jpg";
import hawaiian from "./assets/images/hawaiian.jpg";
import bacon from "./assets/images/bacon.jpg";

let url = "#";

class App extends Component{
  render(){
    return(
      <div>
      <div className='container'>

          <Header/>

          <div id="slider">
              <div className="text-slider">
                  <h3 className="text-center font-weight-bold font-italic"><i className="fas fa-pizza-slice"></i>  PIZZA 365 !!! <i className="fas fa-pizza-slice"></i></h3>
                  <h5 className="text-center font-weight-bold font-italic">Truly Italian!!</h5>
              </div>

                        <Carousel>
                          <Carousel.Item>
                            <img
                              className="d-block w-100 img-radius"
                              src={slider1}
                              alt="First slide"
                            />
                          </Carousel.Item>

                          <Carousel.Item>
                            <img
                              className="d-block w-100 img-radius"
                              src={slider2}
                              alt="Second slide"
                            />
                          </Carousel.Item>

                          <Carousel.Item>
                            <img
                              className="d-block w-100 img-radius"
                              src={slider3}
                              alt="Third slide"
                            />
                          </Carousel.Item>

                          <Carousel.Item>
                            <img
                              className="d-block w-100 img-radius"
                              src={slider4}
                              alt="Forth slide"
                            />
                          </Carousel.Item>
                        </Carousel>

          </div>

          <div className="form-contain">
              <div id="body-content">
                  <div className="text-slider text-center">
                      <h4 className="text-border text-center font-weight-bold">Tại sao bạn nên chọn mua tại Pizza 365 !!</h4>
                  </div>
              </div>
              <div className="row">
                  <div className="col-sm-3 border border-warning bg-content-1">
                      <div className="text-content">
                          <h3>Đa dạng</h3>
                          <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay</p>
                      </div>
                  </div>
                  <div className="col-sm-3 border border-warning bg-content-2">
                      <div className="text-content">
                          <h3>Chất lượng</h3>
                          <p>Nguyên liệu sạch 100%, nguồn gốc xuất sứ rõ ràng, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm</p>
                      </div>
                  </div>
                  <div className="col-sm-3 border border-warning bg-content-3">
                      <div className="text-content">
                          <h3>Hương vị</h3>
                          <p>Đảm bảo hương vị ngon, độc đáo, lạ mà bạn chỉ có thể thưởng thức được tại Pizza 365.</p>
                      </div>
                  </div>
                  <div className="col-sm-3 border border-warning bg-content-4">
                      <div className="text-content">
                          <h3>Dịch vụ</h3>
                          <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chóng, chất lượng tân tiến</p>
                      </div>
                  </div>

              </div>
          </div>

          <div className="form-contain">
                      <div className="text-slider text-center">
                          <h4 className="text-border text-center font-weight-bold">Chọn size pizza</h4>
                          <p className="mt-n2">Chọn combo pizza phù hợp với nhu cầu của bạn</p>
                      </div>
              
                      <div className="card-deck d-flex justify-content-around">
                          <div className="card">
                              <div className="card-header text-center bg-warning">
                                  <h4 className="card-title">S (Small)</h4>
                              </div>
                              <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                      <li className="list-group-item">Đường kính <b>20 cm</b></li>
                                      <li className="list-group-item">Sườn nướng <b>2</b></li>
                                      <li className="list-group-item">Salad <b>200 gram</b></li>
                                      <li className="list-group-item">Nước ngọt <b>2</b></li>
                                      <h2 className="card-text font-weight-bold">150.000</h2>
                                      <p className="card-text">(VND)</p>
                                  </ul>
                              </div>
                              <div className="card-footer">
                                  <button id="btn-pizza-s" className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header text-center bg-warning">
                                  <h4 className="card-title">M (Medium)</h4>
                              </div>
                              <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                      <li className="list-group-item">Đường kính <b>25 cm</b></li>
                                      <li className="list-group-item">Sườn nướng <b>4</b></li>
                                      <li className="list-group-item">Salad <b>300 gram</b></li>
                                      <li className="list-group-item">Nước ngọt <b>3</b></li>
                                      <h2 className="card-text font-weight-bold">200.000</h2>
                                      <p className="card-text">(VND)</p>
                                  </ul>
                        
                              </div>
                              <div className="card-footer">
                                  <button id="btn-pizza-m" className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                              </div>
                            </div>
                            <div className="card">
                              <div className="card-header text-center bg-warning">
                                  <h4 className="card-title">L (Large)</h4>
                              </div>
                              <div className="card-body text-center">
                                  <ul className="list-group list-group-flush">
                                      <li className="list-group-item">Đường kính <b>30 cm</b></li>
                                      <li className="list-group-item">Sườn nướng <b>8</b></li>
                                      <li className="list-group-item">Salad <b>500 gram</b></li>
                                      <li className="list-group-item">Nước ngọt <b>4</b></li>
                                      <h2 className="card-text font-weight-bold">250.000</h2>
                                      <p className="card-text">(VND)</p>
                                  </ul>
                            
                              </div>
                              <div className="card-footer">
                                  <button id="btn-pizza-l" className="btn btn-warning w-100 font-weight-bold">Chọn</button>
                              </div>
                            </div>
                                                   
                      </div>
              
                      <div className="form-contain">
                          <div className="text-slider text-center">
                              <h4 className="text-border text-center font-weight-bold">Chọn loại pizza</h4>
                          </div>
                          <div className="card-deck d-flex justify-content-around">
                              <div className="card">
                                  <img className="card-img-top" src={seafood} alt="Card image cap"/>
                                  <div className="card-body">
                                      <h4 className="card-title">OCEAN MANIA</h4>
                                      <p className="card-subtitle text-secondary">PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                      <p className="card-text">Xốt cà chua, phô mai Mozzarella, tôm mực, thanh cua và hành tây</p>
                                      <button id="btn-pizza-ocean" className="btn btn-warning w-100">Chọn</button>
                                  </div>
                                </div>
                                <div className="card">
                                  <img className="card-img-top" src={hawaiian} alt="Card image cap"/>
                                  <div className="card-body">
                                      <h4 className="card-title">HAWAIIAN</h4>
                                      <p className="card-subtitle text-secondary">PIZZA JAMBON DỨA KIỂU HAWAII</p>
                                      <p className="card-text">Xốt cà chua, phô mai Mozzarella, thịt jambon và thơm</p>
                                      <button id="btn-pizza-hawaiian" className="btn btn-warning w-100">Chọn</button>
                                  </div>
                                </div>
                                <div className="card">
                                  <img className="card-img-top" src={bacon} alt="Card image cap"/>
                                  <div className="card-body">
                                      <h4 className="card-title">CHESSY CHICKEN BACON</h4>
                                      <p className="card-subtitle text-secondary">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                      <p className="card-text">Xốt phô mai, thịt gà, thịt heo muối, phô mai Mozzarella và cà chua</p>
                                      <button id="btn-pizza-bacon" className="btn btn-warning w-100">Chọn</button>
                                  </div>
                                </div>
                          </div>
                      </div>              
                  </div>

          <div className="form-contain">
              <div className="text-slider text-center">
              <h4 className="text-border text-center font-weight-bold">Chọn đồ uống</h4>
          </div>
            <select name="drink" id="select-drink" className="form-control">
                  <option value="none">Chọn loại nước uống</option>
                  <option value="pepsi">PEPSI</option>
                  <option value="coca">Cocacola</option>
                  <option value="lavie">Nước suối Lavie</option>    
              </select>
          </div>

          <div className="form-contain">
              <div className="text-slider text-center">
                  <h4 className="text-border text-center font-weight-bold">Gửi đơn hàng</h4>
              </div>   
              <form id="form-login">
                  <div className="row font-weight-bold">                   
                          <label for="name">Tên :</label>                                  
                          <input type="text" name="name" id="inp-name" className="form-control" placeholder="Nhập tên"/>                
                  </div>
                  <div className="row font-weight-bold">                   
                          <label for="name">Email :</label>           
                          <input type="text" name="name" id="inp-email" className="form-control" placeholder="Nhập Email"/>
                  </div>
                  <div className="row font-weight-bold">
                          <label for="name">Phone :</label>
                          <input type="text" name="name" id="inp-phone" className="form-control" placeholder="Nhập số điện thoại"/>
                  </div>
                  <div className="row font-weight-bold">
                          <label for="name">Địa chỉ :</label>
                          <input type="text" name="name" id="inp-address" className="form-control" placeholder="Nhập địa chỉ"/>
                  </div>
                  <div className="row font-weight-bold">
                          <label for="name">Voucher :</label>   
                          <input type="text" name="name" id="inp-voucher" className="form-control" placeholder="Nhập mã voucer (nếu có)"/>
                  </div>
                  <div className="row font-weight-bold">
                          <label for="name">Lời nhắn :</label>
                          <input type="text" name="name" id="inp-message" className="form-control" placeholder="Nhập lời nhắn của bạn"/>
                  </div>
                  <div className="row">
                      <button id="btn-send-data" className="btn btn-warning w-100 font-weight-bold ">Gửi đơn</button>
                  </div>
              </form>
              
          </div>

          </div>

            <Footer/>

    </div> 
    )
  }
}
  
export default App;
